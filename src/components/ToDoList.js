import React, { Component } from 'react';
import gql from 'graphql-tag';
import { graphql, compose } from 'react-apollo';

const TODOS_QUERY = gql`
  query ToDosQuery {
    todos {
      id,
      title
    }
  }
`;

const ADD_MUTATION = gql`
  mutation AddMutation($input: ToDoInput) {
    addToDo(input: $input) {
      title
    }
  }
`;

const DELETE_MUTATION = gql`
  mutation DeleteMutation($id: ID!) {
    deleteToDo(id: $id) {
      id
    }
  }
`;

class ToDoList extends Component {
  state = { title: '' };

  _addToDo = () => {
    this.props.addMutation({
      variables: {
        input: {
          title: this.state.title
        }
      },
      refetchQueries: [{ query: TODOS_QUERY }]
    });
    this.setState({ title: '' });
  }

  _deleteToDo(id) {
    this.props.deleteMutation({
      variables: {
        id: id
      },
      refetchQueries: [{ query: TODOS_QUERY }]
    });
  }

  render() {
    if (this.props.data && this.props.data.loading) {
      return <div>Loading...</div>
    }

    const toDoList = this.props.data.todos;
    
    return (
      <div>
        <input
          type="text"
          value={this.state.title}
          onChange={e => this.setState({ title: e.target.value })}
        />
        <button
          type="submit"
          onClick={() => this._addToDo()}
        >
          ADD
        </button>
        {
          toDoList.map(
            todo => (
              <p key={todo.id}>
                {todo.title}
                &nbsp;&nbsp;
                <span>
                  <button onClick={() => this._deleteToDo(todo.id)}>DEL</button>
                </span>
              </p>
            )
          )
        }
      </div>
    );
  }
}

export default compose(graphql(TODOS_QUERY), graphql(ADD_MUTATION, { name: 'addMutation' }), graphql(DELETE_MUTATION, { name: 'deleteMutation' }))(ToDoList);