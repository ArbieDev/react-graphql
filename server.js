const express = require('express');
const cors = require('cors');
const graphqlHTTP = require('express-graphql');
const { buildSchema } = require('graphql');

const schema = buildSchema(`
  type ToDo {
    id: ID!
    title: String!
  }

  type Query {
    todos: [ToDo]
  }

  input ToDoInput {
    title: String!
  }

  type Mutation {
    addToDo(input: ToDoInput): ToDo
    deleteToDo(id: ID!): ToDo
  }
`);

let fakeDatabase = [];

const root = {
  todos: () => fakeDatabase,
  addToDo: ({input}) => {
    const todo = {
      id: new Date().getTime().toString(),
      title: input.title
    }
    fakeDatabase.push(todo);
    return todo;
  },
  deleteToDo: ({id}) => {
    fakeDatabase.forEach((data, index) => {
      if (data.id === id) {
        delete fakeDatabase[index];
      }
    });
  }
};

const app = express();

app.use(cors());

app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true
}));

app.listen(4000);